package com.tcs.scholasticintegration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.properties")
public class AppProperties {

	// CXF Endpoint parameters
	@Value("${cxf.address.url}")
	private String cxfAddressUrl;

	// REST Endpoint url
	@Value("${rest.service.url}")
	private String restServiceUrl;

	public String getCxfAddressUrl() {
		return cxfAddressUrl;
	}

	public void setCxfAddressUrl(String cxfAddressUrl) {
		this.cxfAddressUrl = cxfAddressUrl;
	}

	public String getRestServiceUrl() {
		return restServiceUrl;
	}

	public void setRestServiceUrl(String restServiceUrl) {
		this.restServiceUrl = restServiceUrl;
	}

}
