package com.tcs.scholasticintegration.camel;

import javax.jws.WebService;

/* Update the service name used in the endpoint URI */
@WebService(serviceName="service")
public interface CXFService {

	public Object serviceOperationMethod();
	
}
