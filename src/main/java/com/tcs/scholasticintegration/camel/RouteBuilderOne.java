package com.tcs.scholasticintegration.camel;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tcs.scholasticintegration.AppProperties;

@Component
public class RouteBuilderOne extends RouteBuilder {

	@Autowired
	private AppProperties properties;

	
	@Override
	public void configure() throws Exception {
		
		onException(Exception.class).handled(true).stop();
		
		from("cxf:bean:cxfEndpoint").
		log("LOG RECEIVED MESSAGE <AUDIT>").
		/* Processor used for any query elements needed for Rest Service call.*/
		process(new Processor() {
		    public void process(Exchange exchange) throws Exception {
		        
		    	String payload = exchange.getIn().getBody(String.class);
		        // do something with the payload and/or exchange here
		        exchange.getIn().setBody("Changed body");
		   
		    }
		}).to(properties.getRestServiceUrl()).
		log("LOG SENT MESSAGE <AUDIT>").
		end();
		
	}

}
