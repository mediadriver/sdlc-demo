package com.tcs.scholasticintegration;

import org.apache.camel.CamelContext;
import org.apache.camel.CamelContextAware;
import org.apache.camel.component.cxf.CxfEndpoint;
import org.apache.camel.component.cxf.DataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.tcs.scholasticintegration.camel.CXFService;

@Configuration
public class AppConfiguration implements CamelContextAware {

	@Autowired
	private AppProperties properties;

	private CamelContext context;

	@Bean
	public CxfEndpoint cxfEndpoint() {

		CxfEndpoint cxfEndpoint = new CxfEndpoint();
		cxfEndpoint.setAddress(properties.getCxfAddressUrl());
		cxfEndpoint.setServiceClass(CXFService.class);
		cxfEndpoint.setCamelContext(context);
		cxfEndpoint.setDataFormat(DataFormat.PAYLOAD);

		return cxfEndpoint;
	}

	public CamelContext getCamelContext() {
		return context;
	}

	public void setCamelContext(CamelContext context) {
		this.context = context;
	}
}
